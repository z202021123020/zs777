package view.studentclass;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import dao.ClassDao;
import model.StudentClass;
import util.StrUtil;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

public class ClassListFrame extends JInternalFrame {
	private JTable classListTable;
	private JTextField className;
	private JTextField classGrade;
	private JTextField classSecondary;
	private JTextField classMajor;
	private JButton editButton;
	private JButton deleteButton;
	
	private DefaultTableModel dtm=null;

	/**
	 * Create the frame.
	 */
	public ClassListFrame() {
		
		setClosable(true);
		setIconifiable(true);
		
		setBounds(100, 100, 896, 588);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 864, 418);
		getContentPane().add(scrollPane);
		
		classListTable = new JTable();
		classListTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectRow(e);
			}
		});
		
		classListTable.getTableHeader().setReorderingAllowed(false);
		
		classListTable.setRowHeight(25);
		
		DefaultTableCellRenderer r=new DefaultTableCellRenderer();
		r.setHorizontalAlignment(JLabel.CENTER);
		classListTable.setDefaultRenderer(Object.class, r);
		classListTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"\u73ED\u7EA7\u7F16\u53F7", "\u6240\u5C5E\u5E74\u7EA7", "\u73ED\u7EA7\u540D\u79F0", "\u6240\u5C5E\u5B66\u9662", "\u6240\u5C5E\u4E13\u4E1A", "\u73ED\u7EA7\u4FE1\u606F"
			}
		) {
			public boolean isCellEditable(int row,int colum) {
				return false;
			}
		});
		classListTable.getColumnModel().getColumn(0).setPreferredWidth(100);
		classListTable.getColumnModel().getColumn(0).setMinWidth(100);
		classListTable.getColumnModel().getColumn(0).setMaxWidth(100);
		classListTable.getColumnModel().getColumn(2).setPreferredWidth(140);
		classListTable.getColumnModel().getColumn(2).setMinWidth(140);
		classListTable.getColumnModel().getColumn(2).setMaxWidth(140);
		classListTable.getColumnModel().getColumn(3).setPreferredWidth(160);
		classListTable.getColumnModel().getColumn(3).setMinWidth(160);
		classListTable.getColumnModel().getColumn(3).setMaxWidth(160);
		classListTable.getColumnModel().getColumn(4).setPreferredWidth(160);
		classListTable.getColumnModel().getColumn(4).setMinWidth(160);
		classListTable.getColumnModel().getColumn(4).setMaxWidth(160);
		classListTable.getColumnModel().getColumn(5).setPreferredWidth(210);
		classListTable.getColumnModel().getColumn(5).setMinWidth(210);
		classListTable.getColumnModel().getColumn(5).setMaxWidth(210);
		scrollPane.setViewportView(classListTable);
		
		JLabel lblNewLabel = new JLabel("\u73ED\u7EA7\u540D\u79F0");
		lblNewLabel.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/\u540D\u79F0.png")));
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel.setBounds(10, 458, 85, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u6240\u5C5E\u5B66\u9662");
		lblNewLabel_1.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/\u5B66\u9662.png")));
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(10, 502, 85, 15);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("\u6240\u5C5E\u4E13\u4E1A");
		lblNewLabel_1_1.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/\u4E13\u4E1A.png")));
		lblNewLabel_1_1.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		lblNewLabel_1_1.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1_1.setBounds(260, 502, 74, 15);
		getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("\u6240\u5C5E\u5E74\u7EA7");
		lblNewLabel_1_1_1.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/grade.png")));
		lblNewLabel_1_1_1.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1_1_1.setBounds(260, 458, 74, 15);
		getContentPane().add(lblNewLabel_1_1_1);
		
		className = new JTextField();
		className.setBounds(105, 455, 145, 21);
		getContentPane().add(className);
		className.setColumns(10);
		
		classGrade = new JTextField();
		classGrade.setColumns(10);
		classGrade.setBounds(344, 455, 145, 21);
		getContentPane().add(classGrade);
		
		classSecondary = new JTextField();
		classSecondary.setColumns(10);
		classSecondary.setBounds(105, 499, 145, 21);
		getContentPane().add(classSecondary);
		
		classMajor = new JTextField();
		classMajor.setColumns(10);
		classMajor.setBounds(344, 499, 145, 21);
		getContentPane().add(classMajor);
		
		JButton btnNewButton = new JButton("\u641C\u7D22");
		btnNewButton.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/\u641C\u7D22.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectSomeClass();
			}
		});
		btnNewButton.setBounds(514, 454, 97, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u91CD\u7F6E");
		btnNewButton_1.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/_\u91CD\u7F6E.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetButton();
			}
		});
		btnNewButton_1.setBounds(514, 498, 97, 23);
		getContentPane().add(btnNewButton_1);
		
		deleteButton = new JButton("\u5220\u9664");
		deleteButton.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/\u5220\u9664.png")));
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteButtun(e);
			}
		});
		deleteButton.setBounds(648, 454, 97, 23);
		getContentPane().add(deleteButton);
		
		editButton = new JButton("\u7F16\u8F91");
		editButton.setIcon(new ImageIcon(ClassListFrame.class.getResource("/images/\u7F16\u8F91.png")));
		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		editButton.setBounds(648, 498, 97, 23);
		getContentPane().add(editButton);
		
		this.dtm=(DefaultTableModel) classListTable.getModel();
		
		queryAllClass();

	}
	
	protected void deleteButtun(ActionEvent e) {
		// TODO Auto-generated method stub
		if(JOptionPane.showConfirmDialog(this, "是否删除此班级？","正在删除班级……",JOptionPane.YES_NO_CANCEL_OPTION)==JOptionPane.OK_OPTION) {
			String id=dtm.getValueAt(this.classListTable.getSelectedRow(), 0).toString();
			ClassDao classDao=new ClassDao();
			JOptionPane.showMessageDialog(this, classDao.deleteStudentClass(id));
			queryAllClass();
		}
	}

	protected void selectRow(MouseEvent e) {
		// TODO Auto-generated method stub
		this.className.setText(dtm.getValueAt(this.classListTable.getSelectedRow(),2).toString());
		this.classGrade.setText(dtm.getValueAt(this.classListTable.getSelectedRow(),1).toString());
		this.classSecondary.setText(dtm.getValueAt(this.classListTable.getSelectedRow(),4).toString());
		this.classMajor.setText(dtm.getValueAt(this.classListTable.getSelectedRow(),3).toString());
		this.editButton.setEnabled(true);
		this.deleteButton.setEnabled(true);
	}

	protected void selectSomeClass() {
		// TODO Auto-generated method stub
		String name=this.className.getText();
		String grade=this.classGrade.getText();
		String secondary=this.classSecondary.getText();
		String major=this.classMajor.getText();
		if(StrUtil.isEmpty(name)&&StrUtil.isEmpty(grade)&&StrUtil.isEmpty(secondary)&&StrUtil.isEmpty(major)) {
			queryAllClass();
			return;
		}
		StudentClass tempClass=new StudentClass();
		tempClass.setName(name);
		tempClass.setGrade(grade);
		tempClass.setSecondary(secondary);
		tempClass.setMajor(major);
		
		dtm.setRowCount(0);
		ClassDao classDao=new ClassDao();
		List<StudentClass> allClassList=classDao.querySomeClass(tempClass);
		
		for(StudentClass stc:allClassList) {
			Vector v=new Vector();
			v.add(stc.getId());
			v.add(stc.getGrade());
			v.add(stc.getName());
			v.add(stc.getSecondary());
			v.add(stc.getMajor());
			v.add(stc.getInfo());
			dtm.addRow(v);
		}
	}

	protected void resetButton() {
		// TODO Auto-generated method stub
		this.className.setText("");
		this.classGrade.setText("");
		this.classSecondary.setText("");
		this.classMajor.setText("");
	}

	public void queryAllClass() {
		dtm.setRowCount(0);
		ClassDao classDao=new ClassDao();
		List<StudentClass> allClassList=classDao.queryAllClass();
		
		for(StudentClass stc:allClassList) {
			Vector v=new Vector();
			v.add(stc.getId());
			v.add(stc.getGrade());
			v.add(stc.getName());
			v.add(stc.getSecondary());
			v.add(stc.getMajor());
			v.add(stc.getInfo());
			dtm.addRow(v);
		}
		this.editButton.setEnabled(false);
		this.deleteButton.setEnabled(false);
	}
	
	public void doDefaultCloseAction() {
		this.setVisible(false);
		resetButton();
	}
}
