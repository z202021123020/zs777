package view.studentclass;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextPane;

import model.CollegeStructure;
import view.IndexFrame;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.AncestorListener;

import dao.ClassDao;
import model.StudentClass;

import javax.swing.event.AncestorEvent;
import javax.swing.ImageIcon;

public class AddClassFrame extends JInternalFrame {
	private JTextField classNameText;
	private JComboBox classGradeComb; 
	private JComboBox classSecondaryComb;
	private JComboBox classMajorComb;
	private JTextArea classInfo;



	/**
	 * Create the frame.
	 */
	public AddClassFrame() {
		setTitle("\u6B63\u5728\u6DFB\u52A0\u73ED\u7EA7");
		setClosable(true);
		setIconifiable(true);
		
		setBounds(100, 100, 587, 423);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u73ED\u7EA7\u540D\uFF1A");
		lblNewLabel.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/\u540D\u79F0.png")));
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel.setBounds(53, 70, 73, 15);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u6240\u5C5E\u5E74\u7EA7");
		lblNewLabel_1.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/grade.png")));
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(276, 70, 83, 15);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("\u6240\u5C5E\u5B66\u9662");
		lblNewLabel_1_1.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/\u5B66\u9662.png")));
		lblNewLabel_1_1.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1_1.setBounds(43, 139, 83, 15);
		getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("\u6240\u5C5E\u4E13\u4E1A");
		lblNewLabel_1_2.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/\u4E13\u4E1A.png")));
		lblNewLabel_1_2.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1_2.setBounds(276, 139, 83, 15);
		getContentPane().add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_1_3 = new JLabel("\u73ED\u7EA7\u4ECB\u7ECD");
		lblNewLabel_1_3.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/\u4ECB\u7ECD.png")));
		lblNewLabel_1_3.setFont(new Font("宋体", Font.PLAIN, 13));
		lblNewLabel_1_3.setBounds(53, 200, 73, 15);
		getContentPane().add(lblNewLabel_1_3);
		
		classNameText = new JTextField();
		classNameText.setBounds(136, 67, 105, 21);
		getContentPane().add(classNameText);
		classNameText.setColumns(10);
		
		classGradeComb = new JComboBox(new DefaultComboBoxModel(CollegeStructure.graderStr));
		classGradeComb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		classGradeComb.setBounds(369, 66, 122, 23);
		getContentPane().add(classGradeComb);
		
		classSecondaryComb = new JComboBox(new DefaultComboBoxModel(CollegeStructure.secondaryStr));
		classSecondaryComb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddClassFrame.this.classMajorComb.setModel
				(new DefaultComboBoxModel(CollegeStructure.major[AddClassFrame.this.classSecondaryComb.getSelectedIndex()]));
			}
		});
		classSecondaryComb.setBounds(136, 135, 122, 23);
		getContentPane().add(classSecondaryComb);
		
		classMajorComb = new JComboBox(new DefaultComboBoxModel(CollegeStructure.major[0]));
		classMajorComb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		classMajorComb.setBounds(369, 135, 122, 23);
		getContentPane().add(classMajorComb);
		
		classInfo = new JTextArea();
		classInfo.setLineWrap(true);
		classInfo.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		classInfo.setBounds(136, 200, 330, 95);
		getContentPane().add(classInfo);
		
		JButton btnNewButton = new JButton("\u786E\u8BA4");
		btnNewButton.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/\u786E\u8BA4.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addClassButton(e);
			}
		});
		btnNewButton.setBounds(237, 317, 97, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u91CD\u7F6E");
		btnNewButton_1.setIcon(new ImageIcon(AddClassFrame.class.getResource("/images/_\u91CD\u7F6E.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetButton();
			}
		});
		btnNewButton_1.setBounds(369, 317, 97, 23);
		getContentPane().add(btnNewButton_1);
		
		btnNewButton.setFocusable(false);
		btnNewButton_1.setFocusable(false);

	}



	protected void addClassButton(ActionEvent e) {
		// TODO Auto-generated method stub
		String name=this.classNameText.getText();
		String grade=this.classGradeComb.getSelectedItem().toString();
		String secondary=this.classSecondaryComb.getSelectedItem().toString();
		String major=this.classMajorComb.getSelectedItem().toString();
		String info=this.classInfo.getText();
		
		String id=CollegeStructure.majorNum[classSecondaryComb.getSelectedIndex()][classMajorComb.getSelectedIndex()]+String.valueOf(Integer.parseInt(grade)%2000);
		
		StudentClass tempClass=new StudentClass(id,grade,name,secondary,major,info);
		
		ClassDao classDao=new ClassDao();
		JOptionPane.showMessageDialog(this, classDao.addStudentClass(tempClass));
		
		if(IndexFrame.classListFrame!=null) {
			IndexFrame.classListFrame.queryAllClass();
		}
	}



	protected void resetButton() {
		// TODO Auto-generated method stub
		this.classNameText.setText("");
		this.classGradeComb.setSelectedIndex(0);
		this.classSecondaryComb.setSelectedIndex(0);
		this.classMajorComb.setModel(new DefaultComboBoxModel(CollegeStructure.major[0]));
		this.classInfo.setText("这个班级很懒，啥也没留下");
	}
	
	public void doDefaultCloseAction() {
		this.setVisible(false);
		this.resetButton();
	}
}
