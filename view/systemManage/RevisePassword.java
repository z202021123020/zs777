package view.systemManage;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.AncestorListener;

import dao.AdminDao;
import util.StrUtil;
import view.IndexFrame;

import javax.swing.event.AncestorEvent;

public class RevisePassword extends JInternalFrame {
	private JTextField oldPasswordField;
	private JTextField newPasswordField;
	private JTextField againPasswordField;



	/**
	 * Create the frame.
	 */
	public RevisePassword() {
		setBounds(100, 100, 450, 300);
		setClosable(true);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u5F53\u524D\u7528\u6237\uFF1A");
		lblNewLabel.setBounds(107, 38, 73, 36);
		getContentPane().add(lblNewLabel);
		
		String userTypeStr=IndexFrame.userType.getName();
		String adminNameStr=IndexFrame.admin.getName();
		
		JLabel lbladmin = new JLabel("【"+userTypeStr+"】"+adminNameStr);
		lbladmin.addAncestorListener(new AncestorListener() {
			public void ancestorAdded(AncestorEvent event) {
			}
			public void ancestorMoved(AncestorEvent event) {
			}
			public void ancestorRemoved(AncestorEvent event) {
			}
		});
		lbladmin.setForeground(new Color(0, 0, 139));
		lbladmin.setBounds(167, 38, 149, 36);
		getContentPane().add(lbladmin);
		
		JLabel lblNewLabel_1 = new JLabel("\u65E7\u5BC6\u7801\uFF1A");
		lblNewLabel_1.setBounds(107, 84, 73, 36);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("\u65B0\u5BC6\u7801\uFF1A");
		lblNewLabel_1_1.setBounds(107, 128, 73, 36);
		getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		lblNewLabel_1_1_1.setBounds(95, 174, 65, 36);
		getContentPane().add(lblNewLabel_1_1_1);
		
		oldPasswordField = new JTextField();
		oldPasswordField.setBounds(160, 92, 156, 21);
		getContentPane().add(oldPasswordField);
		oldPasswordField.setColumns(10);
		
		newPasswordField = new JTextField();
		newPasswordField.setColumns(10);
		newPasswordField.setBounds(160, 136, 156, 21);
		getContentPane().add(newPasswordField);
		
		againPasswordField = new JTextField();
		againPasswordField.setColumns(10);
		againPasswordField.setBounds(160, 182, 156, 21);
		getContentPane().add(againPasswordField);
		
		JButton btnNewButton = new JButton("\u786E\u8BA4");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmButton(e);
			}
		});
		btnNewButton.setBounds(95, 220, 97, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u91CD\u7F6E");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetButton();
			}
		});
		btnNewButton_1.setBounds(231, 220, 97, 23);
		getContentPane().add(btnNewButton_1);

		setVisible(true);
	}
	
	protected void confirmButton(ActionEvent e) {
		// TODO Auto-generated method stub
		String oldPassword=this.oldPasswordField.getText();
		String newPassword=this.newPasswordField.getText();
		String againPassword=this.againPasswordField.getText();
		if(StrUtil.isEmpty(oldPassword)) {
			JOptionPane.showMessageDialog(this, "请输入原密码");
			return;
		}
		if(StrUtil.isEmpty(newPassword)) {
			JOptionPane.showMessageDialog(this, "请输入新密码");
			return;
		}
		if(StrUtil.isEmpty(againPassword)) {
			JOptionPane.showMessageDialog(this, "请再输入密码");
			return;
		}
		
		if("系统管理员".equals(IndexFrame.userType.getName())) {
			AdminDao adminDao=new AdminDao();
			JOptionPane.showMessageDialog(this, adminDao.revisePassword(IndexFrame.admin, newPassword));
			resetButton();
			return;
		}if("学生".equals(IndexFrame.userType.getName())) {
			return;
		}if("教师".equals(IndexFrame.userType.getName())) {
			return;
		}
		
	}

	protected void resetButton() {
		// TODO Auto-generated method stub
		this.oldPasswordField.setText("");
		this.newPasswordField.setText("");
		this.againPasswordField.setText("");
	}

	public void doDefaultCloseAction() {
		setVisible(false);
	}
}
