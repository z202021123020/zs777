package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import dao.AdminDao;
//import dao.BaseDao;
import model.Admin;
import model.UserType;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField adminName;
	private JTextField adminPassword;
	private JComboBox adminType; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame frame = new LoginFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		setTitle("\u767B\u5F55\u9875\u9762");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u5B66\u751F\u4FE1\u606F\u7BA1\u7406\u7CFB\u7EDF");
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 30));
		lblNewLabel.setBounds(97, 10, 261, 61);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("  \u7528\u6237\u540D\uFF1A");
		lblNewLabel_1.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/\u7528\u6237-\u89D2\u8272-\u7528\u6237\u540D-\u5355\u4EBA_jurassic - \u526F\u672C.png")));
		lblNewLabel_1.setBounds(61, 96, 107, 18);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("    \u5BC6\u7801\uFF1A");
		lblNewLabel_1_1.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/\u5BC6\u7801.png")));
		lblNewLabel_1_1.setBounds(61, 127, 107, 18);
		contentPane.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("\u7528\u6237\u7C7B\u578B\uFF1A");
		lblNewLabel_1_1_1.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/\u7528\u6237\u7C7B\u578B.png")));
		lblNewLabel_1_1_1.setBounds(61, 159, 107, 18);
		contentPane.add(lblNewLabel_1_1_1);
		
		adminName = new JTextField();
		adminName.setBounds(181, 96, 152, 21);
		contentPane.add(adminName);
		adminName.setColumns(10);
		
		adminPassword = new JPasswordField();
		adminPassword.setColumns(10);
		adminPassword.setBounds(181, 127, 152, 21);
		contentPane.add(adminPassword);
		
		adminType = new JComboBox();
		adminType.setModel(new DefaultComboBoxModel(new UserType[] {UserType.ADMIN,UserType.STUDENT,UserType.TEACHER}));
		adminType.setBounds(181, 158, 169, 23);
		contentPane.add(adminType);
		
		JButton btnNewButton = new JButton("\u6CE8\u518C");
		btnNewButton.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/\u6CE8\u518C.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(7, 219, 97, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u767B\u5F55");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				confirmButtom(e);
			}
		});
		btnNewButton_1.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/\u786E\u8BA4.png")));
		btnNewButton_1.setBounds(326, 219, 97, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("\u91CD\u7F6E");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetButton(e);
			}
		});
		btnNewButton_2.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/_\u91CD\u7F6E.png")));
		btnNewButton_2.setBounds(114, 219, 97, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("\u53D6\u6D88");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_3.setIcon(new ImageIcon(LoginFrame.class.getResource("/images/\u53D6\u6D88.png")));
		btnNewButton_3.setBounds(221, 219, 97, 23);
		contentPane.add(btnNewButton_3);
		
		btnNewButton.setFocusable(false);
		btnNewButton_1.setFocusable(false);
		btnNewButton_2.setFocusable(false);
		btnNewButton_3.setFocusable(false);
		
		setLocationRelativeTo(null);
	}

	protected void confirmButtom(ActionEvent e) {
		// TODO Auto-generated method stub
		String name = this.adminName.getText();
		String password = this.adminPassword.getText();
		
		UserType userType =(UserType) this.adminType.getSelectedItem();
		
		if("系统管理员".equals(userType.getName())) {
			AdminDao adminDao = new AdminDao();
			
			Admin admin = adminDao.selectAdmin(name, password);
			
			if(admin==null) {
				JOptionPane.showMessageDialog(this, "用户名或密码错误");
				return;
			}
			
			IndexFrame indexFrame=new IndexFrame(userType,admin);
			indexFrame.setVisible(true);
			this.dispose();
		}
		if("学生".equals(userType.getName())) {
			return;
		}
		if("教师".equals(userType.getName())){
			return;
		}
	}

	protected void resetButton(ActionEvent e) {
		// TODO Auto-generated method stub
		this.adminName.setText("");
		this.adminPassword.setText("");
		this.adminType.setSelectedIndex(0);
	}
}
