package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Admin;
import model.UserType;
import view.studentclass.AddClassFrame;
import view.studentclass.ClassListFrame;
import view.systemManage.RevisePassword;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IndexFrame extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;
	
	public static RevisePassword revisePassword=null;
	public static ClassListFrame classListFrame=null;
	public static AddClassFrame addClassFrame=null;
	
	public static UserType userType;
	public static Admin admin;



	/**
	 * Create the frame.
	 */
	public IndexFrame(UserType u,Admin a) {
		
		userType=u;
		admin=a;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		setLocationRelativeTo(null);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("\u7CFB\u7EDF\u7BA1\u7406");
		mnNewMenu.setIcon(new ImageIcon(IndexFrame.class.getResource("/images/\u8BBE\u7F6E.png")));
		mnNewMenu.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 15));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("\u4FEE\u6539\u5BC6\u7801");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				revisePassword(e);
			}
		});
		mntmNewMenuItem.setIcon(new ImageIcon(IndexFrame.class.getResource("/images/\u4FEE\u6539\u5BC6\u7801.png")));
		mntmNewMenuItem.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 14));
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("\u9000\u51FA\u7CFB\u7EDF");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mntmNewMenuItem_1.setIcon(new ImageIcon(IndexFrame.class.getResource("/images/\u9000\u51FA.png")));
		mntmNewMenuItem_1.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 14));
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenu mnNewMenu_1 = new JMenu("\u73ED\u7EA7\u7BA1\u7406");
		mnNewMenu_1.setIcon(new ImageIcon(IndexFrame.class.getResource("/images/\u73ED\u7EA7\u7BA1\u7406.png")));
		mnNewMenu_1.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 15));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("\u6DFB\u52A0\u73ED\u7EA7");
		mntmNewMenuItem_2.setIcon(new ImageIcon(IndexFrame.class.getResource("/images/\u6DFB\u52A0.png")));
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addStudentClass(e);
			}
		});
		mntmNewMenuItem_2.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 14));
		mnNewMenu_1.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_1_1 = new JMenuItem("\u73ED\u7EA7\u5217\u8868");
		mntmNewMenuItem_1_1.setIcon(new ImageIcon(IndexFrame.class.getResource("/images/\u5217,\u5217\u8868.png")));
		mntmNewMenuItem_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				studentClassListButton(e);
			}
		});
		mntmNewMenuItem_1_1.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 14));
		mnNewMenu_1.add(mntmNewMenuItem_1_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(new Color(255, 250, 240));
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}


	
	protected void studentClassListButton(ActionEvent e) {
		// TODO Auto-generated method stub
		if(classListFrame==null) {
			classListFrame= new ClassListFrame();
			desktopPane.add(classListFrame);
		}
		classListFrame.setBounds((970-classListFrame.getWidth())/2,(600-classListFrame.getHeight())/2,900,600);
		classListFrame.setVisible(true);
	}



	protected void addStudentClass(ActionEvent e) {
		// TODO Auto-generated method stub
		if(addClassFrame==null) {
			addClassFrame=new AddClassFrame();
			desktopPane.add(addClassFrame);
		}
		addClassFrame.setBounds((970-addClassFrame.getWidth())/2,(600-addClassFrame.getHeight())/2,530,380);
		addClassFrame.setVisible(true);
	}



	protected void revisePassword(ActionEvent e) {
		// TODO Auto-generated method stub
		if(revisePassword==null) {	
			revisePassword=new RevisePassword();
			desktopPane.add(revisePassword);
		}
		revisePassword.setBounds((970-revisePassword.getWidth())/2,(600-revisePassword.getHeight())/2,370,300);
		revisePassword.setVisible(true);
	}
}
