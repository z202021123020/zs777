package model;

public enum CollegeStructure {
	JISUANJI("计算机工程学院",0),
	HANGHAI("航海学院",1),
	LUNJI("轮机工程学院",2),
	SHUICHAN("水产学院",3),
	SHIGONG("食品与生物工程学院",4),
	TIYU("体育学院",5),
	CAIJING("财经学院",6),
	JIAOSHIJIAOYU("教师教育学院",7),
	GONGSHANG("工商管理学院",8),
	YINYUE("音乐学院",9),
	MEISHU("美术学院",10),
	XINGONG("信息工程学院",11),
	JIXIE("机械与能源工程学院",12),
	LIXUE("理学院",13),
	FAXUE("法学院",14),
	WAIYU("外国语学院",15),
	WENXUE("文学院",16),
	GONGCHENG("工程技术学院",17),
	MASI("马克思主义学院",18),
	JIJIAO("继续教育学院",19),
	HAWAIJIAOYU("海外教育学院",20);
	
	private String name;
	private int index;
	
	private CollegeStructure(String name,int index) {
		this.name=name;
		this.index=index;
	}
	
	
	
	public static final String [][] major= {
			{"计算机科学与技术","软件工程","网络工程","信息管理与信息系统"},
			{"航海技术","交通运输","物流管理"},
			{"轮机工程","船舶与海洋化工程"},
			{"水产养殖学"},
			{"食品科学与工程"},
			{"体育教育"},
			{"经济学"},
			{"小学教育"},
			{"工商管理"},
			{"音乐表演"},
			{"动画"},
			{"电子信息工程"},
			{"机械设计制造及其自动化"},
			{"数学与应用数学"},
			{"英语"},
			{"法学"},
			{"汉语言文学"},
			{"港口航道与海岸工程"},
			{},
			{},
			{}
	};
	
	public static final String [][] majorNum= {
			{"080901","080902","080903","20102"},
			{"081803","081801","120601"},
			{"081804","081901"},
			{"090601"},
			{"082701"},
			{"040201"},
			{"020101"},
			{"040107"},
			{"120201"},
			{"130201"},
			{"130310"},
			{"080701"},
			{"080202"},
			{"070101"},
			{"050201"},
			{"030101"},
			{"050101"},
			{"081103"},
			{},
			{},
			{}
	};
	
	public static final String[] graderStr= {"2019","2020","2021"};
	
	public static final String[] secondaryStr= {
			JISUANJI.getName(),
			HANGHAI.getName(),
			LUNJI.getName(),
			SHUICHAN.getName(),
			SHIGONG.getName(),
			TIYU.getName(),
			CAIJING.getName(),
			JIAOSHIJIAOYU.getName(),
			GONGSHANG.getName(),
			YINYUE.getName(),
			MEISHU.getName(),
			XINGONG.getName(),
			JIXIE.getName(),
			LIXUE.getName(),
			FAXUE.getName(),
			WAIYU.getName(),
			WENXUE.getName(),
			GONGCHENG.getName(),
			MASI.getName(),
			JIJIAO.getName(),
			HAWAIJIAOYU.getName()
	};

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}
	
}