package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {
	private static ReadProperties rp;
	
	public String dbUrl;
	public String dbUsername;
	public String dbPassword;
	
	private ReadProperties() {
		loadProperties();
	}
	
	public static ReadProperties initial() {
		if(rp==null)
			rp=new ReadProperties();
		return rp;
	}
	
	private void loadProperties() {
		InputStream ips=getClass().getResourceAsStream("/db.properities.txt");
		Properties properties=new Properties();
		
		try {
			properties.load(ips);
			this.dbUrl=properties.getProperty("URL");
			this.dbUsername=properties.getProperty("userName");
			this.dbPassword=properties.getProperty("password");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
