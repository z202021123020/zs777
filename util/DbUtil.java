package util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbUtil {
	
	private static ReadProperties rp=ReadProperties.initial();
	public static Connection getConnection() {
		try {
			Connection connection=DriverManager.getConnection(rp.dbUrl,rp.dbUsername,rp.dbPassword);
			return connection;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	
	}

}
