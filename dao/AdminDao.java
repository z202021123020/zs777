package dao;

import java.sql.*;
import java.sql.SQLException;

import model.Admin;
import view.IndexFrame;

public class AdminDao extends BaseDao{
	public Admin selectAdmin(String name,String password) {
		String sqlStr = "select * from s_admin where name = ? and password = ?";
		Admin admin=null;
		try {
			this.pStatement = this.con.prepareStatement(sqlStr);
			this.pStatement.setString(1, name);
			this.pStatement.setString(2, password);
			
			ResultSet executeQuery = this.pStatement.executeQuery();
			if(executeQuery.next()) {
				admin = new Admin(executeQuery.getInt(1),executeQuery.getString(2),executeQuery.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.close();
		}
		return admin;
	}

	public String revisePassword(Admin admin,String newPassword) {
		String resultStr="用户名或密码错误";
		String sqlStr="update s_admin set password = ? where name = ? and password = ?";
		try {
			this.pStatement=this.con.prepareStatement(sqlStr);
			this.pStatement.setString(1, newPassword);
			//this.pStatement.setInt(2, admin.getId());
			this.pStatement.setString(2, admin.getName());
			this.pStatement.setString(3, admin.getPassword());
			if(this.pStatement.executeUpdate()>0) {
				resultStr="操作成功";
				IndexFrame.admin.setPassword(newPassword);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.close();
		}
		return resultStr;
	}

}
