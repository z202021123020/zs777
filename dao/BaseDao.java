package dao;

import java.sql.*;

import util.DbUtil;

public abstract class BaseDao {
	
	protected Connection con=DbUtil.getConnection();
	
	protected PreparedStatement pStatement=null;
	
	protected void close() {
		try {
			this.con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
