package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.StudentClass;

public class ClassDao extends BaseDao{
	
	public String addStudentClass(StudentClass tempClass) {
		String resultStr="����ʧ��";
		
		String sqlStr1="select count(*) from s_class where id like '"+tempClass.getId()+"%'";
		String sqlStr2="select * from s_class where id=?";
		String sqlStr3="insert into s_class value(?,?,?,?,?,?)";
		
		try {
			this.pStatement=this.con.prepareStatement(sqlStr1);
			ResultSet executeQuery=this.pStatement.executeQuery();
			int count=-1;
			if(executeQuery.next()) {
				count=executeQuery.getInt(1);
			}
			if(count==-1) {
				return resultStr;
			}
			
			this.pStatement=this.con.prepareStatement(sqlStr2);
			this.pStatement.setString(1,tempClass.getId()+ ++count);
			executeQuery = this.pStatement.executeQuery();
			while(executeQuery.next()) {
				this.pStatement.setString(1, tempClass.getId()+ ++count);
				executeQuery=this.pStatement.executeQuery();
			}
			
			tempClass.setId(tempClass.getId()+ count);
			
			this.pStatement=this.con.prepareStatement(sqlStr3);
			this.pStatement.setString(1, tempClass.getId());
			this.pStatement.setString(2, tempClass.getGrade());
			this.pStatement.setString(3, tempClass.getName());
			this.pStatement.setString(4, tempClass.getSecondary());
			this.pStatement.setString(5, tempClass.getMajor());
			this.pStatement.setString(6, tempClass.getInfo());
			if(this.pStatement.executeUpdate()>0) {
				resultStr="���ӳɹ�";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.close();
		}
		
		return resultStr;
	}
	
	public ArrayList <StudentClass> queryAllClass() {
		ArrayList<StudentClass> array = new ArrayList();
		String sqlStr="select * from s_class";
		try {
			this.pStatement=this.con.prepareStatement(sqlStr);
			ResultSet executeQuery=this.pStatement.executeQuery();
			while(executeQuery.next()) {
				StudentClass tempClass=new StudentClass();
				tempClass.setId(executeQuery.getString(1));
				tempClass.setGrade(executeQuery.getString(2));
				tempClass.setName(executeQuery.getString(3));
				tempClass.setSecondary(executeQuery.getString(4));
				tempClass.setMajor(executeQuery.getString(5));
				tempClass.setInfo(executeQuery.getString(6));
				array.add(tempClass);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.close();
		}
		
		return array;
	}

	public ArrayList <StudentClass> querySomeClass(StudentClass tempClass){
		ArrayList<StudentClass> arrays = new ArrayList<StudentClass>();
		String sqlStr="select * from s_class where name like '%"+tempClass.getName()+"%' and grade like '%"+tempClass.getGrade()+"%' and secondary like '%"+tempClass.getSecondary()+"%' and major like '"+tempClass.getMajor()+"%'";
		try {
			this.pStatement=this.con.prepareStatement(sqlStr);
			ResultSet executeQuery=this.pStatement.executeQuery();
			while(executeQuery.next()) {
				StudentClass tempClass2=new StudentClass();
				tempClass2.setId(executeQuery.getString(1));
				tempClass2.setGrade(executeQuery.getString(2));
				tempClass2.setName(executeQuery.getString(3));
				tempClass2.setSecondary(executeQuery.getString(4));
				tempClass2.setMajor(executeQuery.getString(5));
				tempClass2.setInfo(executeQuery.getString(6));
				arrays.add(tempClass2);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.close();
		}
		
		return arrays;
	}
	
	public String deleteStudentClass(String id) {
		String resultStr="ɾ��ʧ��";
		String sqlStr="delete from s_class where id = ?";
		try {
			this.pStatement=this.con.prepareStatement(sqlStr);
			this.pStatement.setString(1,id);
			if(this.pStatement.executeUpdate()>0) {
				resultStr="ɾ���ɹ�";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			this.close();
		}
		return resultStr;
		
	}
}
